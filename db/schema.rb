# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_28_201026) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "package_comments", force: :cascade do |t|
    t.bigint "package_id", null: false
    t.bigint "parent_id"
    t.bigint "user_id", null: false
    t.text "text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["package_id"], name: "index_package_comments_on_package_id"
    t.index ["parent_id"], name: "index_package_comments_on_parent_id"
    t.index ["user_id"], name: "index_package_comments_on_user_id"
  end

  create_table "package_likes", force: :cascade do |t|
    t.bigint "package_id", null: false
    t.bigint "user_id", null: false
    t.integer "impact", null: false
    t.text "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["package_id", "user_id"], name: "index_package_likes_on_package_id_and_user_id", unique: true
    t.index ["package_id"], name: "index_package_likes_on_package_id"
    t.index ["user_id"], name: "index_package_likes_on_user_id"
  end

  create_table "packages", force: :cascade do |t|
    t.bigint "category_id", null: false
    t.bigint "region_id", null: false
    t.string "package_code"
    t.string "title"
    t.string "url"
    t.string "rap_name"
    t.string "rap_data"
    t.text "description"
    t.string "author"
    t.integer "download_count", default: 0
    t.boolean "approved", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "info"
    t.index ["category_id"], name: "index_packages_on_category_id"
    t.index ["region_id"], name: "index_packages_on_region_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "username"
    t.boolean "admin", default: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "package_comments", "package_comments", column: "parent_id"
  add_foreign_key "package_comments", "packages"
  add_foreign_key "package_comments", "users"
  add_foreign_key "package_likes", "packages"
  add_foreign_key "package_likes", "users"
  add_foreign_key "packages", "categories"
  add_foreign_key "packages", "regions"
end
