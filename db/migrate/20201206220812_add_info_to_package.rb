class AddInfoToPackage < ActiveRecord::Migration[6.0]
  def change
    add_column :packages, :info, :jsonb
  end
end
