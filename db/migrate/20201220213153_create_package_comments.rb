class CreatePackageComments < ActiveRecord::Migration[6.0]
  def change
    create_table :package_comments do |t|
      t.references :package, null: false, foreign_key: true
      t.references :parent, foreign_key: { to_table: :package_comments }
      t.references :user, null: false, foreign_key: true
      t.text :text
      t.integer :effect

      t.timestamps
    end
  end
end
