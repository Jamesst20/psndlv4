class CreatePackages < ActiveRecord::Migration[6.0]
  def change
    create_table :packages do |t|
      t.references :category, null: false, foreign_key: true
      t.references :region, null: false, foreign_key: true
      t.string :package_code
      t.string :title
      t.string :url
      t.string :rap_name
      t.string :rap_data
      t.text :description
      t.string :author
      t.integer :download_count, default: 0
      t.boolean :approved, default: false

      t.timestamps
    end
  end
end
