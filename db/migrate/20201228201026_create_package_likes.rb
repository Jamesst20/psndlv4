class CreatePackageLikes < ActiveRecord::Migration[6.0]
  def change
    create_table :package_likes do |t|
      t.references :package, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :impact, null: false
      t.text :reason

      t.timestamps

      t.index %i[package_id user_id], unique: true
    end
  end
end
