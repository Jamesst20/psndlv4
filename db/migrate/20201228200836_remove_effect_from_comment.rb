class RemoveEffectFromComment < ActiveRecord::Migration[6.0]
  def change
    remove_column :package_comments, :effect, :integer
  end
end
