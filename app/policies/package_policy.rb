class PackagePolicy < ApplicationPolicy
  def index?
    true
  end

  def under_approval?
    true
  end

  def show?
    true
  end

  def create?
    user.present?
  end

  def new?
    create?
  end

  def update?
    user&.admin?
  end

  def edit?
    update?
  end

  def destroy?
    user&.admin?
  end

  def download?
    true
  end

  def download_rap?
    download?
  end

  def database?
    true
  end

  def download_database?
    true
  end
end
