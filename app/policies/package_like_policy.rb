class PackageLikePolicy < ApplicationPolicy
  def like?
    user.present?
  end

  def dislike?
    like?
  end

  def create?
    like?
  end
end
