class HomePolicy < ApplicationPolicy
  def index?
    true
  end

  def faq?
    true
  end

  def help_or_thank?
    true
  end
end
