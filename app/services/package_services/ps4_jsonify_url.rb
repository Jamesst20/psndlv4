module PackageServices
  class Ps4JsonifyUrl
    def initialize(url:)
      @url = url
    end

    def run!
      return url if url.blank? || !url.end_with?('.pkg')

      matches = url.match(/^(.+)(_\d+)(.pkg)$/)

      return url.sub('.pkg', '.json') if matches.nil?

      "#{matches[1]}.json" # Remove the part number just in case and use .json instead.
    end

    private

    attr_reader :url
  end
end
