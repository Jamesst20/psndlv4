module PackageServices
  class ExportCsv
    require 'csv'

    def initialize(packages:)
      @packages = packages
    end

    def run!
      CSV.generate(headers: false, col_sep: ';', encoding: 'UTF-8', quote_char: '|') do |csv|
        packages.each do |package|
          csv << csv_columns.map { |c| map_column(package, c) }
        end
      end
    end

    private

    attr_reader :packages

    def csv_columns
      [:package_code, :title, %i[category name], %i[region code], :url, :rap_name, :rap_data, :description, :author]
    end

    def map_column(package, column)
      value = [column].flatten.inject(package) { |carry, col| carry.send(col) }.try(:strip)

      return if value.blank?

      value = value.gsub("\r", '').gsub("\n", '\n') if column == :description
      value
    end
  end
end
