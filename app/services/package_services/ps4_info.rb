module PackageServices
  # Build PS4 package information.
  # Shouldn't be called directly. Use PackageServices::Info
  class Ps4Info
    def initialize(payload:)
      @payload = payload
    end

    def run!
      {
        url: json_url,
        package_id: package_id,
        platform: 'PS4',
        rap_required: false,
        package_size: file_size,
        parts: payload['pieces'].map { |p| p['url'] }
      }
    end

    private

    attr_reader :payload

    def file_size
      payload['originalFileSize']
    end

    def package_id
      @package_id ||= (json_url.split('/').last || []).split('-')[1]
    end

    def json_url
      @json_url ||= PackageServices::Ps4JsonifyUrl.new(url: payload['pieces'][0]['url']).run!
    end
  end
end
