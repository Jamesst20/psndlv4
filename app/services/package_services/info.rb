module PackageServices
  class Info
    require 'http'

    def initialize(url:)
      @url = url
    end

    def run!
      return if url.blank? || !url_exists?

      return PackageServices::Ps3Info.new(url: url).run! if ps3_pkg?
      return PackageServices::Ps4Info.new(payload: ps4_json_payload).run! if ps4_pkg?

      nil
    end

    def updated_url
      return url if ps3_pkg? || !ps4_pkg?

      jsonified_url
    end

    private

    attr_reader :url

    def fetch_range(fetch_url, from, to)
      output = `curl --silent --range #{from}-#{to} --output - #{fetch_url}`

      return [] if output.length.zero? || output.include?('404 Not Found')

      output.bytes
    end

    def url_exists?
      @url_exists ||= begin
        code = HTTP.follow(strict: false).head(url).code
        code >= 200 && code < 300
      end
    end

    def ps3_pkg?
      @ps3_pkg ||= fetch_range(url, 0, 3)
      @ps3_pkg == [0x7F, 0x50, 0x4B, 0x47]
    end

    def ps4_pkg?
      return false unless jsonified_url.end_with? '.json'
      return false if ps4_json_payload.blank? || !ps4_json_payload.key?('pieces')

      # Seems reliable too: url.include? 'gs2.ww.prod.dl.playstation.net'
      @ps4_pkg ||= fetch_range(ps4_json_payload['pieces'][0]['url'], 0, 3)
      @ps4_pkg == [0x7F, 0x43, 0x4E, 0x54]
    end

    def jsonified_url
      @jsonified_url ||= PackageServices::Ps4JsonifyUrl.new(url: url).run!
    end

    def ps4_json_payload
      @ps4_json_payload ||= HTTP.get(jsonified_url)

      return if @ps4_json_payload.code != 200
      return if @ps4_json_payload.headers['Content-Type'] != 'application/json'

      JSON.parse(@ps4_json_payload.body.to_s)
    end
  end
end
