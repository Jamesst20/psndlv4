module PackageServices
  # Build PS3 package information.
  # Shouldn't be called directly. Use PackageServices::Info to ensure
  # proper validations.
  class Ps3Info
    require 'http'

    def initialize(url:)
      @url = url
    end

    def run!
      {
        url: url,
        package_id: package_id,
        platform: 'PS3',
        rap_required: rap_required?,
        rap_name: rap_name,
        license_type: license_type,
        license_name: license_name,
        package_size: file_size
      }
    end

    private

    attr_reader :url

    def fetch_range(from, to)
      output = `curl --silent --range #{from}-#{to} --output - #{url}`

      return [] if output.length.zero? || output.include?('404 Not Found')

      output.bytes
    end

    ##
    # License : 1 byte
    # Offset : 0xCB (usually). If license = 0x40, then it's a mini --> Offset 0x28B
    # License type : 01 = Network (rap required), 02 = Local (rap required), 03 = free (no rap required)
    ##
    def license_type
      @license_type ||= begin
        byte = fetch_range(0xCB, 0xCB)[0]
        byte = fetch_range(0x28B, 0x28B)[0] if byte == 0x40
        byte
      end
    end

    def license_name
      case license_type
      when 0x01
        'Network license - RAP required. (0x01)'
      when 0x02
        'Local license - RAP required. (0x02)'
      when 0x03
        'Free license - No RAP required. (0x03)'
      else
        'No license or unknown'
      end
    end

    def rap_name
      @rap_name ||= begin
        bytes = fetch_range(48, 84)

        return if bytes.length.zero?

        "#{fetch_range(48, 84).pack('U*').gsub(/[^a-zA-Z0-9\-_]/, '')}.RAP"
      end
    end

    def rap_required?
      [0x01, 0x02].include? license_type
    end

    def file_size
      @file_size ||= HTTP.follow(strict: false).head(url).headers['Content-Length']
    end

    def package_id
      @package_id ||= begin
        return if rap_name.blank?

        rap_name.split('-')[1].split('_')[0]
      end
    end
  end
end
