module ImportationServices
  require 'csv'

  class Psndlv1Csv
    def initialize(csv:)
      @csv = csv
    end

    def run!
      parsed_csv.each_with_index do |row, index|
        next if url_invalid?(row)

        print_progress(index)

        info = PackageServices::Info.new(url: row[headers[:url]]).run!

        package = Package.find_or_initialize_by(url: info.try(:[], :url))
        package.assign_attributes(map_attributes(row))
        package.info = info
        package.save!
      rescue ActiveRecord::RecordInvalid => e
        puts "Failed to import url : #{row[headers[:url]]} (#{e})"
      end
    end

    private

    attr_reader :csv

    def print_progress(index)
      puts "Processing package #{index + 1}/#{parsed_csv.length}"
    end

    def parsed_csv
      @parsed_csv ||= CSV.parse(csv, headers: false, col_sep: ';', encoding: 'UTF-8', quote_char: '|')
    end

    def map_attributes(row) # rubocop:disable Metrics/AbcSize
      {
        package_code: row[headers[:package_id]],
        title: row[headers[:title]],
        category: category_by_name(row[headers[:category]]),
        region: region_by_code(row[headers[:region]]),
        url: row[headers[:url]],
        rap_name: row[headers[:rap_name]],
        rap_data: row[headers[:rap_data]],
        description: row[headers[:description]],
        author: row[headers[:author]]
      }.merge(row[headers[:download_count]].blank? ? {} : { download_count: row[headers[:download_count]] })
    end

    def category_by_name(name)
      @category_by_name ||= {}
      @category_by_name[name] ||= Category.find_or_create_by!(name: name)
      @category_by_name[name]
    end

    def region_by_code(code)
      @region_by_code ||= {}
      @region_by_code[code] ||= begin
        region = Region.find_or_initialize_by(code: code)
        region.update!(name: code) if region.new_record?
        region
      end
      @region_by_code[code]
    end

    def url_invalid?(row)
      row[headers[:url]].blank? || row[headers[:url]].end_with?('.jpg', '.mp4')
    end

    def headers
      {
        package_id: 0,
        title: 1,
        category: 2,
        region: 3,
        url: 4,
        rap_name: 5,
        rap_data: 6,
        description: 7,
        author: 8,
        download_count: 9 # This one is added by myself, it doesn't really exists in PSNDLv1/PSNStuff
      }
    end
  end
end
