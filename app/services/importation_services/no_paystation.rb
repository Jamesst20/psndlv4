module ImportationServices
  class NoPaystation
    require 'csv'

    def initialize(csv:, category:, skip_existing: true, skip_new: false)
      @csv = csv
      @category = category
      @skip_new = skip_new
      @skip_existing = skip_existing
    end

    def run! # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
      validate_headers!

      valid_rows.each_with_index do |row, index|
        print_progress(index)

        package = Package.find_or_initialize_by(url: row['PKG direct link'])

        next if (skip_existing && package.persisted?) || (skip_new && package.new_record?)

        package.assign_attributes(map_attributes(package, row))
        package.info = PackageServices::Info.new(url: row['PKG direct link']).run! if package.new_record?

        raise 'PS4 is not supported because url could change causing a duplicate.' if package.info_platform == 'PS4'

        print_changes(package) if package.persisted?

        package.save!
      rescue ActiveRecord::RecordInvalid => e
        puts "Failed to import url : #{row['PKG direct link']} (#{e})"
      end
    end

    private

    attr_reader :csv, :category, :skip_existing, :skip_new

    def print_progress(index)
      puts "Processing package #{index + 1}/#{parsed_csv.length}"
    end

    def print_changes(package)
      sentence = package.changes.map { |attr, change| "'#{attr}' changed from #{change[0]} to #{change[1]}" }.join(', ')
      puts sentence if sentence.present?
    end

    def parsed_csv
      @parsed_csv ||=  CSV.parse(csv, headers: true, col_sep: "\t", encoding: 'UTF-8', quote_char: '|')
    end

    def valid_rows
      @valid_rows ||= begin
        parsed_csv.reject { |row| row['PKG direct link'].blank? || row['PKG direct link'].exclude?('http') }
      end
    end

    def map_attributes(package, row)
      {
        package_code: row['Title ID'],
        region: region_by_code(row['Region']),
        category: category,
        title: row['Name'],
        rap_name: "#{row['Content ID']}.RAP",
        rap_data: package.rap_data.presence || (row['RAP'].try(:length) == 32 ? row['RAP'] : nil)
      }
    end

    def region_by_code(code)
      @region_by_code ||= {}
      @region_by_code[code] ||= begin
        region = Region.find_or_initialize_by(code: code)
        region.update!(name: code) if region.new_record?
        region
      end
      @region_by_code[code]
    end

    def validate_headers!
      raise "CSV doesn't have expected headers" if parsed_csv.headers != expected_headers
    end

    def expected_headers
      ['Title ID', 'Region', 'Name', 'PKG direct link', 'RAP', 'Content ID', 'Last Modification Date',
       'Download .RAP file', 'File Size', 'SHA256']
    end
  end
end
