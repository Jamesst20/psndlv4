class ApplicationController < ActionController::Base
  include NoticeHelper
  include Pundit

  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action :verify_authorized, unless: :devise_controller?

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protected

  def configure_permitted_parameters
    # Add extra permitted columns to devise for registration
    added_attrs = %i[username email password password_confirmation remember_me]

    devise_parameter_sanitizer.permit(:sign_up, keys: added_attrs)
    devise_parameter_sanitizer.permit(:account_update, keys: added_attrs)
  end

  private

  def user_not_authorized
    return redirect_to new_user_session_url if current_user.blank?

    render :user_not_authorized
  end
end
