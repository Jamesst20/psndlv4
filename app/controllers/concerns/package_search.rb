module PackageSearch
  def handle_search(packages)
    return packages if params[:filter].blank?

    packages = packages.includes(:region, :category).references(:region, :category)
    packages = _search_by_category(packages) if params[:filter] == 'category'
    packages = _search_by_region(packages) if params[:filter] == 'region'
    packages = _search_by_model_attribute(packages) unless %w[category region].include? params[:filter]
    packages = _search_by_text(packages) if params[:search].present?

    packages
  end

  private

  def _search_by_text(packages)
    search_keyword = "%#{params[:search]}%"
    packages
      .where('title ILIKE ?', search_keyword)
      .or(packages.where('package_code ILIKE ?', search_keyword))
      .or(packages.where('author ILIKE ?', search_keyword))
      .or(packages.where('categories.name ILIKE ?', search_keyword))
      .or(packages.where('regions.name ILIKE ? OR regions.code ILIKE ?', search_keyword, search_keyword))
  end

  def _search_by_model_attribute(packages)
    packages.reorder(params[:filter] => params[:order])
  end

  def _search_by_category(packages)
    packages.reorder('categories.name' => params[:order])
  end

  def _search_by_region(packages)
    packages.reorder('regions.name' => params[:order])
  end
end
