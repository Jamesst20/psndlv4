module NoticeHelper
  def create_successful_notice(params = {})
    merge_default! :created, params
    t("activerecord.notices.#{controller_name.classify.underscore}.created_successfully", params)
  end

  def update_successful_notice(params = {})
    merge_default! :updated, params
    t("activerecord.notices.#{controller_name.classify.underscore}.updated_successfully", params)
  end

  def destroy_successful_notice(params = {})
    merge_default! :destroyed, params
    t("activerecord.notices.#{controller_name.classify.underscore}.destroyed_successfully", params)
  end

  def unknown_error_notice
    t('errors.messages.unknown')
  end

  private

  def merge_default!(action, params)
    params.merge! default: t("activerecord.notices.default.#{action}_successfully", model: translated_model_name)
  end

  def translated_model_name
    # namespace = self.class.module_parent == Object ? '' : "#{self.class.module_parent.name}::"
    controller_name.classify.to_s.constantize.model_name.human
  end
end
