class PackageCommentsController < ApplicationController
  # before_action :set_package, only: %i[show edit update destroy]
  # before_action { authorize @packages || @package || Package }

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_package
    @package = Package.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def package_comment_params
    params.require(:package_comment).permit
  end
end
