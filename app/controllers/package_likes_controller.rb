class PackageLikesController < ApplicationController
  before_action :set_package
  before_action { authorize PackageLike }
  before_action { @norobot = true }

  def like
    handle_current_case!('positive')
  end

  def dislike
    handle_current_case!('negative')
  end

  def create
    @package_like = PackageLike.find_or_initialize_by(user: current_user, package: @package)

    if @package_like.persisted? && @package_like.impact == package_like_params[:impact]
      @package_like.destroy!
    else
      return render current_view unless @package_like.update(package_like_params)
    end

    redirect_to package_path(@package)
  end

  private

  def current_view
    package_like_params[:impact] == 'positive' ? :like : :dislike
  end

  def handle_current_case!(impact)
    current = PackageLike.find_or_initialize_by(user: current_user, package: @package)

    return if current.blank? || current.impact != impact

    current.destroy!

    redirect_to package_path(@package)
  end

  def set_package
    @package = Package.find(params[:package_id])
  end

  def package_like_params
    params.require(:package_like).permit(:impact, :reason)
  end
end
