class HomeController < ApplicationController
  before_action { authorize nil, policy_class: HomePolicy }

  def index
  end

  def faq
  end

  def help_or_thank
  end
end
