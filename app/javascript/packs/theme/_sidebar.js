document.addEventListener("DOMContentLoaded", function() {
  const menuToggle = document.getElementById("menu-toggle");
  if (menuToggle) {
    menuToggle.onclick = function(e) {
      e.preventDefault();

      $("#wrapper").toggleClass("sidebar-toggled");
    }
  }
});