const jQuery = require('jquery');

window.jQuery = jQuery;
window.$ = jQuery;

$(document).ready(() => {
  $.ajaxSetup({
    headers: { 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') }
  });
});