class User < ApplicationRecord
  include DeviseUsernameAuthenticable

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable, :trackable,
         :confirmable

  validates :username, uniqueness: true
end
