class PackageLike < ApplicationRecord
  belongs_to :package
  belongs_to :user

  enum impact: { positive: 1, negative: 2 }, _prefix: true

  validates :impact, presence: true
  validates :reason, presence: true, if: -> { impact_negative? }
  validates :package, uniqueness: { scope: %i[package_id user_id] }
end
