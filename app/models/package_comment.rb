class PackageComment < ApplicationRecord
  belongs_to :package
  belongs_to :user
  belongs_to :parent, class_name: 'PackageComment', optional: true

  has_many :child_comments, class_name: 'PackageComment', foreign_key: :parent_id, dependent: :destroy,
                            inverse_of: :parent

  validates :effect, presence: true
  validates :text, presence: true, unless: -> { positive? }

  validate :one_nesting_level
  validate :package_match_parent

  private

  def one_nesting_level
    return if parent.parent_id.blank?

    errors.add :parent, :package_comment_too_much_nesting
  end

  def package_match_parent
    return if parent.blank? || parent.package_id == package_id

    errors.add(:package, :package_mismatch_parent)
  end
end
