module DeviseUsernameAuthenticable
  extend ActiveSupport::Concern

  included do
    before_validation -> { self.username = username&.downcase }

    def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup.to_h
      value = conditions[:email] || conditions[:username]

      return if value.blank?

      query = where(conditions.except(:email, :username))
      query = query.where(email: value.downcase).or(query.where(username: value.downcase))
      query.first
    end
  end
end
