class Region < ApplicationRecord
  validates :code, :name, presence: true
  validates :code, length: { maximum: 3 }
end
