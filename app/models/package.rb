class Package < ApplicationRecord
  belongs_to :category
  belongs_to :region

  has_many :package_likes, dependent: :destroy
  has_many :package_comments, dependent: :destroy

  before_validation :assign_attributes_from_info

  store :info, accessors: %i[url package_id platform rap_required rap_name license_type license_name package_size
                             parts], prefix: true

  validates :package_code, :title, :url, :info, presence: true
  validates :info_url, :info_package_id, :info_platform, :info_package_size, presence: true
  validates :info_rap_name, presence: true, if: -> { info_platform == 'PS3' }
  validates :info_parts, presence: true, if: -> { info_platform == 'PS4' }
  validates :rap_data, presence: true, if: :info_rap_required
  validates :rap_data, length: { minimum: 32, maximum: 32 }, if: -> { rap_data.present? }
  validate :ps4_url_validation

  def ps4?
    info_platform == 'PS4'
  end

  def info=(val)
    super(val)
    assign_attributes_from_info
  end

  private

  def assign_attributes_from_info
    self.package_code = info_package_id
    self.rap_name = info_rap_name
    self.url = info_url
  end

  def ps4_url_validation
    return if info_platform != 'PS4'

    errors.add :url, :ps4_url_json unless url&.end_with? '.json'
  end
end
