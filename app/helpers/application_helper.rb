module ApplicationHelper
  def show_field(model, attribute)
    attribute = [attribute].flatten
    value = attribute.inject(model) { |m, attr| m.send(attr) }
    value = yield value if block_given?
    tag.div(class: 'form-group') do
      [
        tag.label(model.class.human_attribute_name(attribute.first)),
        tag.label(_f(value), class: 'form-control', readonly: true)
      ].join.html_safe # rubocop:disable Rails/OutputSafety
    end
  end

  private

  def _f(text)
    simple_format(text.to_s.presence || '&nbsp;', {}, wrapper_tag: 'div', sanitize: text.present?)
  end
end
