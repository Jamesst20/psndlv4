class PackageDecorator < Draper::Decorator
  delegate_all

  def author
    self['author'] || 'Unknown'
  end

  def success_rate
    total = package_likes.count
    positive = package_likes.where(impact: :positive).count

    return 'N/A' if total.zero?

    "#{(positive.to_f / total * 100).to_i}%"
  end

  def like?(user)
    return false if user.blank?

    PackageLike.find_by(package: self, user: user).try(:impact_positive?) || false
  end

  def dislike?(user)
    return false if user.blank?

    PackageLike.find_by(package: self, user: user).try(:impact_negative?) || false
  end
end
