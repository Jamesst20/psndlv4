Rails.application.routes.draw do
  devise_for :users

  resources :packages do
    resources :package_likes, only: :create do
      collection do
        get :like
        get :dislike
      end
    end
    member do
      get :download
      get :download_rap
    end
    collection do
      get :under_approval
      get :database
      post :download_database
    end
  end

  get :faq, to: 'home#faq'
  get :help_or_thank, to: 'home#help_or_thank'

  root to: 'home#index'
end
